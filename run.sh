#!/bin/sh
APP_FILE=${APP_URL:7}
APP_FILE=${WAR_FILE##*/}
if [ -f /app/app.jar ];
then
  echo "app.jar downloaded. Ignore."
else
  echo "Downloading java app from url $APP_URL"
  wget -O /app/app.jar $APP_URL
fi
cd /app && java $JAVA_OPTS -jar app.jar $APP_OPTS
