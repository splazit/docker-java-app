Running standalone app with OpenJDK and Alpine
==============================================

This image is based on **latest** tag of [OpenJDK Alpine](https://hub.docker.com/_/openjdk/). If you can also specify a jdk version **splazit/javaapp:jdk8** which will use jdk8 of the OpenJDK.

This simple image will download your external application (ie an spring boot app) and then run it. Everything is contained within the container.

There are 3 environment variables you can pass into the image: APP_URL (required), JAVA_OPTS and APP_OPTS.

Usage Example
-------------
Run in an interactive mode and watch the log (if you app logs to the console).
```
$ docker run -it --rm -e APP_URL='https://www.ssosol.com/download/demo.jar' -e JAVA_OPTS='-Duser.timezone=America/Edmonton' splazit/javaapp
```